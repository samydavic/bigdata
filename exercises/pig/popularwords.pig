a1 = LOAD '/dataset/the_adventures_of_sherlock_holmes.txt' using PigStorage(',');
b1 = foreach a1 generate flatten(TOKENIZE((chararray)$0)) as word1;
c1 = group b1 by word1;
d1 = foreach c1 generate COUNT(b1) as count, group;

a2 = LOAD '/dataset/the_adventures_of_tom_sawyer.txt' using PigStorage(',');
b2 = foreach a2 generate flatten(TOKENIZE((chararray)$0)) as word2;
c2 = group b2 by word2;
d2 = foreach c2 generate COUNT(b2) as count, group;

d11 = limit d1 100;
d21 = limit d2 100;

j3 = join d11 by group, d21 by group;
j4 = foreach j3 generate d11::count + d21::count as count, d11::group;
dump j4;

--store d into '/user/hue/pig_wordcount';
--DUMP d1;
--DESCRIBE d1;
--DESCRIBE d2;
--DESCRIBE d3;

