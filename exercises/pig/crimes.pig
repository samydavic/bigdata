Crimes = LOAD '/user/root/crimes' using PigStorage(',') AS (id:int, case_number:chararray, date:chararray, block:chararray, iucr:chararray, primary_type:chararray, description:chararray, location_description:chararray, arrest:chararray, domestic:chararray, beat:chararray, district:chararray, ward:int, community_area:chararray, fbi_code:chararray, x_coordinate:int, y_coordinate:int, year:chararray, updated_on:chararray, latitude:float, longitude:float, location:chararray);
DESCRIBE Crimes;

SimpleCrimes = FILTER Crimes BY description == 'SIMPLE';
DUMP SimpleCrimes;

--Cien = LIMIT SimpleCrimes 100;
--DESCRIBE Cien;
--STORE DUMP SimpleCrimes INTO 'root';

