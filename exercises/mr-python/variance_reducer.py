#!/usr/bin/env python
import sys

def compute_variance(current_column, xiTotal, xi2Total, i):
    mean = xiTotal / i
    variance = xi2Total / i - mean * mean
    print '%s\t%f' % (current_column, variance)

i = int(0)
xiTotal = float(0)
xi2Total = float(0)
column = None
current_column = int(0)

for line in sys.stdin:
    line = line.strip()
    column, value  = line.split('\t', 1)
    column = int(column)
    x = float(value)
    if column == current_column:
        xiTotal += x
        xi2Total += x * x
        i += 1
    else:
        compute_variance(current_column, xiTotal, xi2Total, i)
        current_column = column
        xiTotal = 0
        xi2Total = 0
        i = 0

# ultima columna
compute_variance(column, xiTotal, xi2Total, i)
