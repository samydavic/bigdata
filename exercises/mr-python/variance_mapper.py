#!/usr/bin/env python
import sys

# input comes from STDIN (standard output)
for line in sys.stdin:    
    line = line.strip()
    values = line.split(',')
    column = 0
    # increase counters
    for value in values:
        x = float(value)
        print '%i\t%f' % (column, x)
        column += 1
