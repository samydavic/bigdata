#!/usr/bin/env python
import sys

# input comes from STDIN (standard input)
for line in sys.stdin:
    # remove leading and trailing whitespace
    line = line.strip()

    # split the line into a list of words
    words = line.split(' ')

    # increase counters
    for word in words:
        if word.strip() != "":
            # write the results to STDOUT (standard output);
            # what we output here will be the input for the
            # Reduce step, tab-delimited. 
            # The trivial word count is 1		
            print '%s\t%s' % (word, 1)
