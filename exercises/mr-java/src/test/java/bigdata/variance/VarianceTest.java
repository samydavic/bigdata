package bigdata.variance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class VarianceTest {

    MapDriver<LongWritable, Text, IntWritable, FloatWritable> mapDriver;
    ReduceDriver<IntWritable, FloatWritable, IntWritable, FloatWritable> reduceDriver;

    private static final LongWritable ZERO_OFFSET = new LongWritable(0l);

    @Before
    public void setup() {
        VarianceMapper mapper = new VarianceMapper();
        VarianceReducer reducer = new VarianceReducer();

        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void testMapper() throws IOException {
        Text inValue = new Text("600,140.85,850");
        mapDriver.withInput(ZERO_OFFSET, inValue);

        IntWritable outkey1 = new IntWritable(0);
        FloatWritable outValue1 = new FloatWritable(600f);
        IntWritable outKey2 = new IntWritable(1);
        FloatWritable outValue2 = new FloatWritable(140.85f);
        IntWritable outKey3 = new IntWritable(2);
        FloatWritable outValue3 = new FloatWritable(850f);

        mapDriver.withOutput(outkey1, outValue1);
        mapDriver.withOutput(outKey2, outValue2);
        mapDriver.withOutput(outKey3, outValue3);

        mapDriver.runTest(true);
    }

    @Test
    public void testReducer() throws IOException {
        IntWritable inkey1 = new IntWritable(0);
        FloatWritable inValue1 = new FloatWritable(600f);
        FloatWritable inValue2 = new FloatWritable(470f);
        FloatWritable inValue3 = new FloatWritable(170f);
        FloatWritable inValue4 = new FloatWritable(430f);
        FloatWritable inValue5 = new FloatWritable(300f);
        List<FloatWritable> inValue = new ArrayList<FloatWritable>();
        inValue.add(inValue1);
        inValue.add(inValue2);
        inValue.add(inValue3);
        inValue.add(inValue4);
        inValue.add(inValue5);
        reduceDriver.withInput(inkey1, inValue);

        IntWritable outKey = new IntWritable(0);
        FloatWritable outValue = new FloatWritable(21704f);
        reduceDriver.withOutput(outKey, outValue);

        reduceDriver.runTest(true);
    }
}
