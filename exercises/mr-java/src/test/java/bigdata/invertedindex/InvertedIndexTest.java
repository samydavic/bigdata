package bigdata.invertedindex;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import bigdata.invertedindex.FilenameOffsetWritable;
import bigdata.invertedindex.InvertedIndexMapper;
import bigdata.invertedindex.InvertedIndexReducer;

public class InvertedIndexTest {

	MapDriver<LongWritable, Text, Text, FilenameOffsetWritable> mapDriver;
	ReduceDriver<Text, FilenameOffsetWritable, Text, Text> reduceDriver;

	private static final LongWritable ZERO = new LongWritable(0L);
	private static final Text FILE1 = new Text("somefile");
	

	@Before
	public void setup() {
		InvertedIndexMapper mapper = new InvertedIndexMapper();
		InvertedIndexReducer reducer = new InvertedIndexReducer();

		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	}

	@Test
	public void testMapper() throws IOException {
		Text inValue = new Text("bar foo bar");
		mapDriver.withInput(ZERO, inValue);

		Text outkey1 = new Text("bar");
		FilenameOffsetWritable outValue1 = new FilenameOffsetWritable(FILE1, ZERO);
		Text outKey2 = new Text("foo");
		FilenameOffsetWritable outValue2 = new FilenameOffsetWritable(FILE1, ZERO);
		Text outKey3 = new Text("bar");
		FilenameOffsetWritable outValue3 = new FilenameOffsetWritable(FILE1, ZERO);
		
		mapDriver.withOutput(outkey1, outValue1);
		mapDriver.withOutput(outKey2, outValue2);
		mapDriver.withOutput(outKey3, outValue3);

		mapDriver.runTest(true);
	}

	@Test
	public void testReducer() throws IOException {
		Text inkey1 = new Text("bar");
		FilenameOffsetWritable inValue1 = new FilenameOffsetWritable(FILE1, ZERO);
		FilenameOffsetWritable inValue2 = new FilenameOffsetWritable(FILE1, ZERO);
		List<FilenameOffsetWritable> inValue = new ArrayList<FilenameOffsetWritable>();		
		inValue.add(inValue1);
		inValue.add(inValue2);
		reduceDriver.withInput(inkey1, inValue);
		
		Text outKey = new Text("bar");
		Text outValue = new Text(FILE1 + ":" + ZERO + " " + FILE1 + ":" + ZERO );
		reduceDriver.withOutput(outKey, outValue);

		reduceDriver.run();		
	}
}
