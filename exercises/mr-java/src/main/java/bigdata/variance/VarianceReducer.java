package bigdata.variance;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

public class VarianceReducer extends Reducer<IntWritable, FloatWritable, IntWritable, FloatWritable> {

    @Override
    public void reduce(IntWritable key, Iterable<FloatWritable> values, Context context)
            throws IOException, InterruptedException {
        long i = 0;
        float xiTotal = 0;
        float xi2Total = 0;
        for (FloatWritable columnValue : values) {
            float x = columnValue.get();
            xiTotal += x;
            xi2Total += x * x;
            i++;
        }
        float mean = xiTotal / i;
        float variance = xi2Total / i - mean * mean;

        context.write(key, new FloatWritable(variance));
    }
}
