package bigdata.variance;

import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class VarianceMapper extends Mapper<LongWritable, Text, IntWritable, FloatWritable> {

    private static final String CSV_SEPARATOR = ",";

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString().toLowerCase();
        String[] values = line.split(CSV_SEPARATOR);

        int column = 0;
        for (String columnValue : values) {
            float x = Float.valueOf(columnValue);
            context.write(new IntWritable(column), new FloatWritable(x));
            column++;
        }
    }
}
