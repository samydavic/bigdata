package bigdata.invertedindex;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class FilenameOffsetWritable implements WritableComparable<FilenameOffsetWritable> {

	private Text filename;

	private LongWritable offset;

	public FilenameOffsetWritable() {
		this.setFilename(new Text());
		this.setOffset(new LongWritable());
	}

	public FilenameOffsetWritable(String filename, Long offset) {
		this.filename = new Text(filename);
		this.offset = new LongWritable(offset);
	}

	public FilenameOffsetWritable(Text filename, LongWritable offset) {
		this.filename = filename;
		this.offset = offset;
	}

	public Text getFilename() {
		return filename;
	}

	public void setFilename(Text filename) {
		this.filename = filename;
	}

	public LongWritable getOffset() {
		return offset;
	}

	public void setOffset(LongWritable offset) {
		this.offset = offset;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		filename.write(out);
		offset.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		filename.readFields(in);
		offset.readFields(in);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + ((offset == null) ? 0 : offset.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilenameOffsetWritable other = (FilenameOffsetWritable) obj;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (offset == null) {
			if (other.offset != null)
				return false;
		} else if (!offset.equals(other.offset))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FileOffsetWritable [filename=" + filename + ", offset=" + offset + "]";
	}

	@Override
	public int compareTo(FilenameOffsetWritable o) {
		int cmp = this.filename.compareTo(o.filename);
		if (cmp != 0) {
			return cmp;
		}

		return this.offset.compareTo(o.offset);
	}
}
