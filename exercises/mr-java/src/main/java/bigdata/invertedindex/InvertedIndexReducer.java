package bigdata.invertedindex;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class InvertedIndexReducer extends Reducer<Text, FilenameOffsetWritable, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<FilenameOffsetWritable> values, Context context)
            throws IOException, InterruptedException {
        String outValue = "";
        String filename = null;
        for (FilenameOffsetWritable value : values) {
            if (!value.getFilename().toString().equals(filename)) {
                outValue += value.getFilename() + ":" + value.getOffset() + " ";
            } else {
                outValue += value.getOffset() + " ";
            }
            filename = value.getFilename().toString();
        }
        context.write(key, new Text(outValue));
    }

}
