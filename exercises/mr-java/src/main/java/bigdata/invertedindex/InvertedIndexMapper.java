package bigdata.invertedindex;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

public class InvertedIndexMapper extends Mapper<LongWritable, Text, Text, FilenameOffsetWritable> {
    
	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		String filename = ((FileSplit) context.getInputSplit()).getPath().getName();

        String line = value.toString().toLowerCase();
		String[] words = line.split(" ");
		
		for (String word : words) {
			FilenameOffsetWritable filenameOffsetWritable = new FilenameOffsetWritable(new Text(filename), key); 
			context.write(new Text(word), filenameOffsetWritable);
		}
	}
}
