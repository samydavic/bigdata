# import from mysql with sqoop
sqoop import --connect jdbc:mysql://localhost:3306/test --table crimes

# export to mysql with sqoop
sqoop export --connect jdbc:mysql://localhost:3306/test --table crimes_hdfs --export-dir /user/sam/crimes-dataset/ --mysql-delimiters --input-fields-terminated-by '|' -m 4
