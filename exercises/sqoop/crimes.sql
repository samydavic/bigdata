CREATE TABLE `crimes` (
  `id` int(11) NOT NULL,
  `case_number` varchar(10) DEFAULT NULL,
  `date` varchar(22) DEFAULT NULL,
  `block` varchar(50) DEFAULT NULL,
  `iucr` varchar(4) DEFAULT NULL,
  `primary_type` varchar(50) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `location_description` varchar(50) DEFAULT NULL,
  `arrest` varchar(5) DEFAULT NULL,
  `domestic` varchar(5) DEFAULT NULL,
  `beat` varchar(4) DEFAULT NULL,
  `district` varchar(3) DEFAULT NULL,
  `ward` int(4) DEFAULT NULL,
  `community_area` varchar(2) DEFAULT NULL,
  `fbi_code` varchar(3) DEFAULT NULL,
  `x_coordinate` int(11) DEFAULT NULL,
  `y_coordinate` int(11) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  `updated_on` varchar(22) DEFAULT NULL,
  `latitude` decimal(12,9) DEFAULT NULL,
  `longitude` decimal(12,9) DEFAULT NULL,
  `location` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


LOAD DATA INFILE '/dev/ws/bigdata/Crimes_-_2001_to_present-nonull.csv' INTO TABLE crimes
COLUMNS 
	TERMINATED BY ','
	OPTIONALLY ENCLOSED BY '"'
	ESCAPED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;
